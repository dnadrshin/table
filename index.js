var path = require('path');
var webpack = require('webpack');
var config = require('./webpack.js');
var express = require('express');

var app = express();
var compiler = webpack(config);
var settings = require('./server/settings');
var bodyParser = require('body-parser');

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

app.use('/public', express.static('public'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.use('/api/', require('./server/routes/'));

app.listen(settings.port, function(err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log(`Listening at http://localhost:${settings.port}`);
});
