const
    router = require('express').Router(),
    fs = require('fs'),
    rp = require('request-promise'),

    getServerToken = rp({
            method: 'POST',
            uri: 'https://apidev.omnea.org/auth/login',
            headers: {'content-type': 'application/json'},
            body: {username: "test@test.test", password: "testtest"},
            json: true,
        }),

    getNewToken = (req, res) => getServerToken
        .then(function (parsedBody) {
            fs.writeFile("./server/token.json", JSON.stringify({token: parsedBody.token, token_exp: parsedBody.token_exp}), err => {
                if(err) {
                    reject(err);
                    return err;
                }

                getAccounts(parsedBody, req, res)
                console.log("The token was saved!");
            }); 
        }),

    isTokenExpired = token_exp => token_exp < new Date().getTime() / 1000,

    getLocalToken = () => new Promise((resolve, reject) => fs.readFile('./server/token.json', 'utf8', (err, data) => {
        if (err) return reject(err);
        resolve(JSON.parse(data));
    })),

    getAccounts = (data, req, res) => rp({
            method: 'POST',
            uri: 'https://apidev.omnea.org/ca-internal/accounts',
            headers: {'Authorization': `Bearer ${data.token}`},

            body: {
                order: req.body.order,
                orderBy: req.body.orderBy, //enum (city, company_name, street, zip, plan_name, overall_rating, submit, zurmo_account_id)
                page: parseInt(req.body.page) || 1,
                pageSize: parseInt(req.body.pageSize) || 10,
                search  : req.body.search || '',
                // filter: { // Only advanced search
                //     city: undefined, //or array
                //     company_name: undefined, //or array
                //     street: undefined, //or array
                //     zip: undefined, //or array
                //     plan_name: undefined, //or array
                //     overall_rating: undefined, //or array
                //     submit: undefined, //or array
                // }
            },

            json: true
        })
            .then(function (parsedBody) {
                res.json(parsedBody)
            })
            
            .catch(function (err) {
                console.log(err)
            });


router.post('/accounts', (req, res) => {
    getLocalToken()
        .then(data => isTokenExpired(data.token_exp)
            ? getNewToken(req, res)
            : getAccounts(data, req, res))

        .catch(err => err.code === 'ENOENT'
            ? getNewToken(req, res)
            : console.log('unknown err', err))
});

module.exports = router;
