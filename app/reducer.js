import { combineReducers } from 'redux';
import {initForm as searchInitForm} from './generic/Search';
import tableReducer from './Table/reducer';
import { createForms, combineForms } from 'react-redux-form';
import accountsRest from './Accounts/rest';
import loginRest from './rest';

const
    reducers = {
        table: tableReducer,
        rest: combineReducers(accountsRest.reducers),
        login: combineReducers(loginRest.reducers),

        ...createForms({
            search: searchInitForm,
        }),
    };

export default combineReducers(reducers);
