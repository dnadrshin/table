import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from './reducer';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { combineForms } from 'react-redux-form';
import Accounts from './Accounts';
import rest from './rest.js';

const logger = createLogger({});

const
    store = createStore(reducer, applyMiddleware(thunk, logger)),
    App = () => <Accounts />;

    // store.dispatch(rest.actions.login.post(
    //     {},

    //     {body: JSON.stringify({
    //         username: 'test@test.test.ru',
    //         password: 'testtest',
    //     })},

    //     (err, data) => {
    //         console.log(err, data);
    //         console.log(store.getState());
    //     }
    // ))

ReactDOM.render(<Provider store={store}><App /></Provider>, document.querySelector('#root'));
