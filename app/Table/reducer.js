import _ from 'lodash';
import actions from './actions';
import fixtures from '../settings/fixtures';
import reduce from '../generic/helpers/reduce';

export default (state = {}, action) => reduce(state, action, {
	[actions.types.TABLE_RESET]: () => {
		return {}
	},

	[actions.types.TABLE_TOGGLE_SORTING]: () => {
		return {
			...state,

			[action.key]: {
				...state[action.key],

				sorting: _.get(state[action.key], 'sorting.column') === action.column
					? state[action.key].sorting.order === 'ASC'
						? {column: action.column, order: 'DESC', sortBy: action.sortBy}
						: {column: action.column, order: 'ASC', sortBy: action.sortBy}

					: {column: action.column, order: 'ASC', sortBy: action.sortBy}
			}
		};
	},
});
