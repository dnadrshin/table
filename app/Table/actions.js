import keyMirror from 'keymirror';

const
    toggleSorting = (key, column, sortBy) => ({column, key, sortBy, type: types.TABLE_TOGGLE_SORTING}),
    tableReset = () => ({type: types.TABLE_RESET}),

    types = keyMirror({
        TABLE_TOGGLE_SORTING: null,
        TABLE_RESET         : null,
    });

export default { types, toggleSorting };
