// @flow
import _ from 'lodash';
import React from 'react';
import {connect} from 'react-redux';
import Row from './Row';
import tableActions from './actions';
import { track, actions } from 'react-redux-form';
import { compose, lifecycle, withHandlers } from 'recompose';

const
    extendColumns = columns => columns.concat(
        // {title: 'Edit', name: 'mode edit', isServiceField: true, action: () => {}}, 
        // {title: 'Delete', name: 'delete', isServiceField: true, action: () => {}},
    ),

    iconsStyle = {verticalAlign: 'middle'},

    Table = ({data, columns, columnSettings, module, sorting, toggleSorting}) => <table className="table table-striped">
        <tbody>
            <tr>
                {extendColumns(columns).map((column, i) =>
                <th
                    style={{cursor: 'pointer'}}
                    key={`${module}-header-${i}`}
                    onClick={column.isSorted ? () => sorting(module, column.name, column.sortBy): () => {console.log('not sort')}}
                >
                    {column.title}

                    {column.isSorted && _.get(columnSettings,'sorting.column') !== column.name
                        ? <i className="material-icons" style={iconsStyle}>swap_vert</i>
                        : ''
                    }

                    {_.get(columnSettings,'sorting.column') === column.name
                        ? _.get(columnSettings,'sorting.order') === 'ASC' 
                            ? <i className="material-icons" style={iconsStyle}>arrow_downward</i>
                            : <i className="material-icons" style={iconsStyle}>arrow_upward</i>
                        : ''}
                </th>)}
            </tr>

            {data
                ? data.map((row, i) => <Row data={row} columns={extendColumns(columns)} key={row.omnea_id}/>)
                : <tr><td>no data</td></tr>
            }
        </tbody>
    </table>;

export default compose(
    connect(
        (state, props) => ({
            columnSettings: state.table[props.module],
        }),

       dispatch => ({
           toggleSorting: (module, column, sortBy) => dispatch(tableActions.toggleSorting(module, column, sortBy)),
        })
    ),

    withHandlers({
        sorting: props => (module, column, sortBy) => {
            props.toggleSorting(module, column, sortBy);
            setTimeout(() => props.submit(), 0);
        }
    })
)(Table);
