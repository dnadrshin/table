import React from 'react';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { Control, Form, Field } from 'react-redux-form';
import { track, actions } from 'react-redux-form';

const
    HtmlPagination = props => <div className="col-xs-3 col-xs-offset-6">
            <nav aria-label="Table navigation">
                <ul className="pagination" style={{float: 'right'}}>
                    {props.children}
                </ul>
            </nav>
        </div>,

    Pagination = props => <HtmlPagination>
            {[...Array(props.pageCount).keys()].map(n => <li className="page-item" key={`pagination-${n}`}>
                <a className="page-link" href="#" onClick={props.change(n+1)}>{n+1}</a>
            </li>)}
        </HtmlPagination>;

export default compose(
    connect(
        state => ({
            records: state.records,
        }),

        (dispatch, props) => ({
            change: n => () => {
                dispatch(actions.change('search.page', n));
                setTimeout(() => props.submit(), 0);
            }
        })
    ),
)(Pagination);
