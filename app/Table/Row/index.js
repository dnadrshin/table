import _ from 'lodash';
import React from 'react';
import Icon from './Icon';

const
    Row = ({data, columns}) => <tr>
        {columns.map((column, i) => <td key={`${data.omnea_id}-col-${i}`}>
            {!column.isServiceField
                ? _.get(data, column.propsName)

                : <Icon
                    type={column.name}
                    action={column.action}
                />}
        </td>)}
    </tr>;

export default Row;
