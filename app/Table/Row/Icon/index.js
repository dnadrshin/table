import React from 'react';

const
    Icon = ({type, action}) => <i style={{cursor: 'pointer'}} className="material-icons" onClick={action}>{type}</i>

export default Icon;
