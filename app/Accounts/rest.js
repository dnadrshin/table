import "isomorphic-fetch";
import reduxApi, {transformers} from "redux-api";
import adapterFetch from "redux-api/lib/adapters/fetch";

export default reduxApi({
    accounts: {
        crud: true,
        url: 'api/accounts',

        options: {
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            }
        },
    },
}).use("fetch", adapterFetch(fetch));
