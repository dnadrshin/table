export default [
    {
        title: 'Location_ID',
        propsName: 'omnea_id',
        name: 'omneaId',
        sortBy: 'zurmo_account_id',
        isSorted: true,
    },

    {
        title: 'Company name',
        propsName: 'basic.category.name',
        name: 'companyName',
        sortBy: 'company_name',
        isSorted: true,
    },

    {
        title: 'Street',
        propsName: 'basic.address.street',
        name: 'street',
        sortBy: 'street',
        isSorted: true,
    },

    {
        title: 'Postcode',
        propsName: 'basic.address.zip',
        name: 'zip',
        sortBy: 'zip',
        isSorted: true,
    },
    
    {
        title: 'City/Town',
        propsName: 'basic.address.city',
        name: 'city',
        sortBy: 'city',
        isSorted: true,
    },

    {
        title: 'Phone',
        propsName: 'basic.phone_number',
        name: 'phoneNumber',
    },

    {
        title: 'Plan',
        propsName: 'basic.address.city',
        name: 'plan_name',
        sortBy: 'plan_name',
        isSorted: true,
    },
    
    {
        title: 'Descriptions',
        propsName: 'basic.ohours.description',
        name: 'description',
    },
    
    {
        title: 'Last Submition',
        propsName: 'last_submit',
        name: 'submit',
        sortBy: 'submit',
    },
]
