import _ from 'lodash';
import React from 'react';
import Table from '../Table';
import Pagination from '../Table/Pagination';
import { compose, lifecycle, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import columns from './columns';
import rest from './rest';
import Search from 'generic/Search';
import Loading from 'generic/Loading';
import PageCounter from 'generic/PageCounter';
import TableBottom from 'generic/TableBottom';

const
    Accounts = props => <x-section>
            <Search submit={props.submit} search={props.search}/>
            <Table submit={props.submit} data={props.accounts} columns={columns} module="records" />

            <TableBottom>
                <PageCounter pageData={props.pageData}/>

                {props.pageCount > 1 && <Pagination
                    submit={props.submit}
                    pageCount={props.pageCount}
                    currentPage={props.currentPage}
                />}
            </TableBottom>

            {props.isLoading && <Loading />}
        </x-section>;

export default compose(
    connect(
        state => ({
            accounts   : state.rest.accounts.data.result,
            isLoading  : state.rest.accounts.loading,
            search     : state.search || {},
            sorting    : _.get(state, 'table.records.sorting', {}),
            isPaginate : state.rest.accounts.data.totalSize,
            pageCount  : Math.ceil(state.rest.accounts.data.totalSize/state.rest.accounts.data.pageSize),
            pageData   : state.rest.accounts.data,
        }),

        dispatch => ({
            post: (params, data, cb) => dispatch(rest.actions.accounts.post(params, data, cb)),
        })
    ),

    withHandlers({
        submit: props => () => props.post(
            null,

            {body: JSON.stringify({
                ...props.search,
                order  : props.sorting.order,
                orderBy: props.sorting.sortBy,
            })},

            (err, data)=> {
                console.log(err, data)
        })
    }),

    lifecycle({
        componentDidMount() {
            this.props.post(
                null,
                {body: JSON.stringify(this.props.search)},
    
                (err, data)=> {
                    console.log(err, data)
            })
        }
    })
)(Accounts);
