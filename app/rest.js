import "isomorphic-fetch";
import reduxApi, {transformers} from "redux-api";
import adapterFetch from "redux-api/lib/adapters/fetch";

export default reduxApi({
    login: {
        crud: true,
        url: 'https:/apidev.omnea.org/auth/',

        options: {
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        },
    },
}).use("fetch", adapterFetch(fetch));
