import React from 'react';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { Control, Form, Field } from 'react-redux-form';

const
    PageCounter = props => {
        const
            {page, pageSize, totalSize, resultSize} = props.pageData;
    
        return <div className="col-xs-3 form-row well">
            {`${pageSize*(page - 1) + 1} to ${pageSize*(page -1) + resultSize} of ${Math.ceil(totalSize/pageSize)} Locations`}
        </div>};

export default compose(
    connect(
        state => ({
            records: state.records,
        }),
    ),
)(PageCounter);
