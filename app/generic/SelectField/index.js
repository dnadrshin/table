import _ from 'lodash';
import React from 'react';
import { Field, Control } from 'react-redux-form';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

const
    SelectFiled = ({model, option, submit, list, valueName}) => <Control.select 
            model={model} 
            className="form-control"
            defaultValue="6"
            onChange={() => setTimeout(submit, 0)}
        >
            {list.map(el => <option value={el[valueName]} key={`${model}-${el[valueName]}`}>{el[option]}</option>)}
        </Control.select>;

export default SelectFiled;
