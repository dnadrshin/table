import React from 'react';

const
    TableBottom = props => <x-section>
        <div className="container-fluid">
            <div className="row">
                {props.children}
            </div>
        </div>
    </x-section>;

export default TableBottom;
