import React from 'react';
import { compose, lifecycle, withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import AdvancedSearch from './AdvancedSearch';
import { Control, Form, Field } from 'react-redux-form';
import SelectField from 'generic/SelectField';
import { track, actions } from 'react-redux-form';

const
    initForm = {username: '', firstName: '', surName: '', passwordFirst: '', passwordSecond: ''},

    buttonStyle = {marginLeft: '14px'},

    Records = props => <Form model="search" className="container-fluid form-inline">
        <div className="form-row well">
            <div className="col-xs-6 form-group" style={{float: 'none'}}>
                <div className="input-group form-group">
                    <Control.text model="search.search" placeholder="Search" id="search.search" className="form-control"/>
                    <span className="input-group-btn">
                        <button className="btn btn-secondary" type="button" onClick={props.submit}>Search</button>
                    </span>
                </div>
                <input type="submit" style={{position: 'absolute', left: '-9999px'}} onClick={props.submit}/>
                <div className="input-group form-group">
                    <button className="btn btn-primary" onClick={props.resetSearch} style={buttonStyle}>Reset</button>
                    <button className="btn btn-primary" onClick={props.toggleAdvanced} style={buttonStyle}>Adv.search</button>
                </div>
            </div>
            <div className="col-xs-1 col-xs-offset-5 form-group" style={{float: 'none'}}>
                <SelectField
                    model="search.pageSize"
                    option="option"
                    list={[
                        {value: "1", option: "1"},
                        {value: "2", option: "2"},
                        {value: "3", option: "3"},
                        {value: "4", option: "4"},
                        {value: "5", option: "5"},
                        {value: "6", option: "6"},
                        {value: "7", option: "7"},
                    ]}
                    submit={props.submit}
                    valueName="value"
                />
            </div>
        </div>

        {props.showAdvanced && <AdvancedSearch />}
    </Form>;

export default compose(
    connect(
        state => ({
            records: state.records,
        }),

        (dispatch, props) => ({
            resetSearch: () => {
                dispatch(actions.reset('search'));
                dispatch(actions.reset('table'));
                dispatch(actions.change('search.pageSize', 6));

                setTimeout(() => props.submit(), 0);
            },
        })
    ),

    withState('showAdvanced', 'setShowAdvanced', false),


    withHandlers({
        toggleAdvanced: props => () => props.setShowAdvanced(!props.showAdvanced)
    }),

    lifecycle({
        componentDidMount() {
        }
    })
)(Records);
