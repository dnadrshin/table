import React from 'react';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { Control, Form, Field } from 'react-redux-form';

const
    Records = props => <div className="form-row well">
        <div className="input-group form-group">
            <Control.text className="form-control" model="search.pageSize1" placeholder="Page Size" id="search.pageSize1" />
        </div>

        <div className="input-group form-group">
            <Control.text className="form-control" model="search.pageSize2" placeholder="Page Size" id="search.pageSize2" />
        </div>
        </div>;

export default compose(
    connect(
        state => ({
            records: state.records,
        }),

        {

        }
    ),

    lifecycle({
        componentDidMount() {
        }
    })
)(Records);
