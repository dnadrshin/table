
import _ from 'lodash';
import React from 'react';
import { Field } from 'react-redux-form';
import { connect } from 'react-redux';

const MultipleFiled = ({path, model, option, list, value, listModel}) => <Field model={model}>
    <select>
        {list.map(el => <div value={el[value]} key={el[value]}>{el[option]}</div>)}
    </select>
</Field>

export default connect(
    (state, props) => ({
        list: _.get(state, props.listModel)
    })
)(MultipleFiled);
